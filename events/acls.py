from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_picture(search_term):
    pexels_url = "https://api.pexels.com/v1/search?query=" + search_term

    print(PEXELS_API_KEY)
    headers = {"Authorization": PEXELS_API_KEY}
    r = requests.get(pexels_url, headers=headers)
    r = json.loads(r.content)
    photos = r["photos"]
    if photos and len(photos) > 0:
        return photos


# def get_weather_data(city, state):
#     url = (
#         "http://api.openweathermap.org/geo/1.0/direct?q="
#         + city
#         + ","
#         + state
#         + ",US&limit=5&appid="
#         + OPEN_WEATHER_API_KEY
#     )
#     response = requests.get(url)
#     data = json.loads(response.content)[0]
#     lon = data["lon"]
#     lat = data["lat"]
#     print(data)
#     return {}
