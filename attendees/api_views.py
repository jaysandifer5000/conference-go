from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .models import Attendee
from events.models import Conference, State


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
    ]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    else:
        content = json.loads(request.body)

    # Get the Conference object and put it in the content dict
    try:
        conference = Conference.objects.get(id=conference_id)
        content["conference"] = conference
    except Conference.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
        )

    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeListEncoder,
        safe=False,
    )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, pk):
    if request.method == "GET":
        attendees = Attendee.objects.get(id=pk)
        return JsonResponse(
            attendees,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # copied from create
        content = json.loads(request.body)
    try:
        # new code
        if "state" in content:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
    except State.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid state abbreviation"},
            status=400,
        )

    # new code
    Attendee.objects.filter(id=pk).update(**content)

    # copied from get detail
    attendees = Attendee.objects.get(id=pk)
    return JsonResponse(
        attendees,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )
